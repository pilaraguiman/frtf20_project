%% Portrait project preparation for Robot Studio
% Maria del Pilar Aguilera
% Blanca Zumarraga
% FRTF20 2021 Lund Sweden

%Load the image
image = imread('re_ester-exposito.jpg');
%image = imread('monigote.jpg');
%image = imread('corazon.png');
figure(1)
imshow(image)
title('Original image')

%Convert to gray scale
I = rgb2gray(image);
figure(2)
imshow(I)
title('Gray scale image')

% Filter image to extract only the edges the parameters for the canny allow
% us to only get the width of the edges we desire
w=edge(I,'canny',[.05,.15]);
figure(3)
imshow(w);
title('Edge image')

%% Create the .mod file to be read in Robot studio
% Apply rotation to all the coordinates to match the paper
% Taken into consideration where our workobject is
% Make sure you move all of this targets under the workobject in question
% Otherwise the coordinates would be off
% A3 format (297mmx420mm) but we start 50mm inside so we can use 
% 200mm in X direction and 300mm in Y direction
% ______________________
% TODO
% ______________________
numrows = 200;
numcols = 200;
img_resize = imresize(w,[numrows numcols],'nearest');
%img_resize = w(1:200,1:200);
figure(5)
imshow(img_resize)
title('Resized image')
axis on
xlabel('X')
ylabel('Y')
% If there is less than 1 pixels connected we delete the object
img_resize = bwareaopen(img_resize, 1);
% Find the coordinates of the resized image
[row_resized,column_resized]= find(img_resize==1);
coordinates_resized = [row_resized,column_resized];
% First add z coordinate of 0 to all the coordinates
img_z = [coordinates_resized zeros(size(coordinates_resized,1),1)];
% Open the file and transfer
fid = fopen('robotstudiofileester.txt', 'wt');
fprintf(fid, 'Module Module1\n');   %First intro line
%Now we write all the robot targets
for c=1:size(coordinates_resized,1) % trial with only 5
    string_name = sprintf("ID_%02d",c);
    fprintf(fid, '\tCONST robtarget %s:=[[%g, %g, %g],[1,0,0,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n',string_name,img_z(c,:));
end
% We add a predefined home target
fprintf(fid, '\tCONST robtarget home:=[[147.048899517,75,-528.200896245],[0,0.608761445,0,0.793353328],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n');
% We add the main
fprintf(fid, '\tPROC main()\n');
fprintf(fid, '\t\tPath_portrait;\n');
fprintf(fid, '\tENDPROC\n');
% We create our path
fprintf(fid, '\tPROC Path_portrait()\n');
fprintf(fid, '\t\tMoveJ home,v100,z5,pen\\WObj:=Workobject_1;\n');
random_1 = randi([1 size(row_resized,1)]);
m = row_resized(random_1);
n = column_resized(random_1);
last = false;
while(true)
    if img_resize(m-1,n-1)==1
        m_new = m-1;
        n_new = n-1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m,n-1)==1
        m_new = m;
        n_new = n-1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m-1,n)==1
        m_new = m-1;
        n_new = n;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m+1,n-1)==1
        m_new = m+1;
        n_new = n-1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m-1,n+1)==1
        m_new = m-1;
        n_new = n+1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m,n+1)==1
        m_new = m;
        n_new = n+1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m+1,n)==1
        m_new = m+1;
        n_new = n;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    elseif img_resize(m+1,n+1)==1
        m_new = m+1;
        n_new = n+1;
        for c=1:size(coordinates_resized,1)
            if(m_new==img_z(c) && n_new==img_z(c,2))
                string_name = sprintf("ID_%02d",c);
                if last == true
                   fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                   last = false;
                end
                fprintf(fid, '\t\tMoveL Offs(%s,0,0,15),v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
                img_resize(m,n)=0;
                img_resize(m_new,n_new)=0;
                m = m_new;
                n = n_new;
            end
        end
    % There is not a single point to paint anymore 
    elseif isempty(img_resize) 
            % Move back to home and close the path and module
            fprintf(fid, '\t\tMoveJ home,v100,z5,pen\\WObj:=Workobject_1;\n');
            fprintf(fid, '\tENDPROC\n');
            fprintf(fid, 'ENDMODULE\n');
            fclose(fid);
            break;
    % Esto es una guarrada...si quedan menos de 50 puntos que no los
    % pinte.. se queda pillado generalmente cuando quedan pocos puntos
    elseif size(row_resized(:,1))<137
        % Move back to home and close the path and module
        fprintf(fid, '\t\tMoveJ home,v100,z5,pen\\WObj:=Workobject_1;\n');
        fprintf(fid, '\tENDPROC\n');
        fprintf(fid, 'ENDMODULE\n');
        fclose(fid);
        break;
    % There was not a 1 surronding the previous one then we have to put the
    % pen up and generate a random value again
    else
        fprintf(fid, '\t\tMoveL %s,v100,fine,pen\\WObj:=Workobject_1;\n',string_name);
        [row_resized,column_resized]= find(img_resize==1);
        random_1 = randi([1 size(row_resized,1)]);
        m = row_resized(random_1);
        n = column_resized(random_1);
        last = true;
    end
end

%% Find the equation of a plane giving 3 points
% The points
% Start plane
P1 = [150,150,-40];
P2 = [0,150,-40];
P3 = [150,0,-40];
% Displaced plane
P7 = [148,136,-39];
P8 = [13,143,-12];
P9 = [149,10,-38];
% Normal vector of the plane start
normal_s = cross(P1-P2, P1-P3);

% Normal vector of the plane finish
normal_f = cross(P7-P8, P7-P9);

% Calculate the euler angle
CosTheta = max(min(dot(normal_s,normal_f)/(norm(normal_s)*norm(normal_f)),1),-1);
ThetaInDegrees = real(acosd(CosTheta));

% Graphical representation 
points_s=[P1' P2' P3']; % using the data given in the question
fill3(points_s(1,:),points_s(2,:),points_s(3,:),'r')
grid on
alpha(0.3)
hold on
points_f=[P7' P8' P9']; 
fill3(points_f(1,:),points_f(2,:),points_f(3,:),'r')
alpha(0.3)
hold off

% Calculate the quaternion
r = vrrotvec(normal_s,normal_f);
rot_mat = vrrotvec2mat(r);
quat2 = rotm2quat(rot_mat)