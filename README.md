# FRTF20_Project

Project for the subject FRTF20 Applied Robotics in Lund University.  

## Name
Robot portrait painting by Pilar Aguilera and Blanca Zumarraga

## Description
The project consisted of a robot arm that had to be able to draw a portrait from a picture in different frames, i.e, it should paint on a flat table and it should also paint on a surface that is tilted. The robot that we used is an ABB IRB 140 and we also used a tool that had been used both as a pen and as a calibrator. The calibration part was used to know the position of the surface on which we were painting to detect the plane orientation in case it was tilted. To develop the project, we used the programming programs Matlab and RobotStudio. Therefore, the project was divided into two main parts. First, the design of a program that can paint any portrait based on a picture. This involved several steps such as image processing to extract all the points to be painted and the moving commands necessary to move the robot. And secondly, the tool with the TCP method and surface calibration was done in RobotStudio.

